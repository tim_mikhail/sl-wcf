﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MessagesRegister.Web
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MessagesService" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select MessagesService.svc or MessagesService.svc.cs at the Solution Explorer and start debugging.
	public class MessagesService : IMessagesService
	{
		public void Register(Message msg)
		{
			msg.RegDate = DateTime.Now;
			msg.Id = Guid.NewGuid();
			MessageRepository.Instance.Add(msg);
		}

		public IEnumerable<Message> Get()
		{
			return MessageRepository.Instance.Get();
		} 
	}
}
