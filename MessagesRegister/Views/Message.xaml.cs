﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessagesRegister.ViewModel;

namespace MessagesRegister
{
	public partial class Message 
	{

		private MessageViewModel _vm;

		public Message()
		{
			InitializeComponent();
		}

	
		// Executes when the user navigates to this page.
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			_vm = new MessageViewModel();
			ListenViewModelEvents();
			TextboxMessage.TextChanged += Textbox_TextChanged;
		}


		void Textbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			var textBox = sender as TextBox;
			if (textBox == null) return;
			var expression = textBox.GetBindingExpression(TextBox.TextProperty);
			if (expression == null) return;
			expression.UpdateSource();
		}

		private void ListenViewModelEvents()
		{
			_vm.GetMessageSuccess += () =>
			{
				LayoutRoot.DataContext = _vm;
			};
			_vm.GetMessage();

			LayoutRoot.DataContext = _vm;

			_vm.SaveMessageFailed += err =>
			{
				AppHelper.ShowMessage("Error", err);
			};

			_vm.SaveMessageSuccess += () =>
			{
			AppHelper.ShowMessage("успешно", "Зарегистрировано сообщение");
				if (NavigationService != null) NavigationService.Navigate(new Uri("/Home", UriKind.RelativeOrAbsolute));
			};
		}

		private void ButtonBack_Click(object sender, RoutedEventArgs e)
		{
			if (NavigationService != null && NavigationService.CanGoBack)
				NavigationService.GoBack();
		}

		//private void ButtonSave_Click(object sender, RoutedEventArgs e)
		//{
		//	_vm.RegisterMessage(TextboxMessage.Text);
		//}
	}
}