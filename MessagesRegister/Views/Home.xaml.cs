﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessagesRegister.ViewModel;

namespace MessagesRegister
{
	public partial class Home : Page
	{
		public Home()
		{
			InitializeComponent();
		}

		// Executes when the user navigates to this page.
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			var vm = new MessagesViewModel();

			vm.LoadMessagesSuccess += employees =>
			{
				LayoutRoot.DataContext = vm;
				var view = new PagedCollectionView(employees);
				DataGridMessages.ItemsSource = view;
			};

			vm.LoadMessagesFailed += reason =>
			{
				AppHelper.ShowMessage(string.Empty, reason);
			};
			vm.LoadMessages();

		}

		private void ButtonNewMessage_Click(object sender, RoutedEventArgs e)
		{
			if (NavigationService != null) NavigationService.Navigate(new Uri("/Message", UriKind.RelativeOrAbsolute));
		}
	}
}