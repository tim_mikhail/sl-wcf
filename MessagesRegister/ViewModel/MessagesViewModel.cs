﻿

using System;
using System.Collections.ObjectModel;
using System.Linq;
using MessagesRegister.Data;
using MessagesRegister.Model;
using MessagesRegister.ServiceReference1;
using MessagesRegister.Util;
using IMessagesService = MessagesRegister.Model.IMessagesService;

namespace MessagesRegister.ViewModel
{
	public class MessagesViewModel : ViewModelBase
	{
		private readonly IMessagesService _service;

		public MessagesViewModel()
			: this(new MessageService())
		{
		}

		public MessagesViewModel(IMessagesService msgService)
        {
            _service = msgService;
			_service.GetMessagesError += OnGetMessagesFailed;
            _service.GetMessageCompleted += ServiceOnGetMessageCompleted;
        }

		private void ServiceOnGetMessageCompleted(ObservableCollection<ServiceReference1.Message> messages)
		{
			var msgs = new ObservableCollection<MessageDto>();
			messages.ToList().ForEach(e => msgs.Add(e.AsMessageDto()));

			OnLoadMessagesSuccess(msgs);
		}
		
		public void LoadMessages()
		{
			
			_service.GetMessages();
		}

		#region Events
		

		static readonly EventKey LoadMessagesFailedEventKey = new EventKey();

		public event Action<string> LoadMessagesFailed
		{
			add { EventSet.Add(LoadMessagesFailedEventKey, value); }
			remove { EventSet.Remove(LoadMessagesFailedEventKey, value); }
		}

		void OnGetMessagesFailed(string failReason)
		{
			EventSet.Raise(LoadMessagesFailedEventKey, failReason);
		}

		static readonly EventKey LoadMessagesSuccessEventKey = new EventKey();

		public event Action<ObservableCollection<MessageDto>> LoadMessagesSuccess
		{
			add { EventSet.Add(LoadMessagesSuccessEventKey, value); }
			remove { EventSet.Remove(LoadMessagesSuccessEventKey, value); }
		}

		void OnLoadMessagesSuccess(ObservableCollection<MessageDto> employees)
		{
			EventSet.Raise(LoadMessagesSuccessEventKey, employees);
		}
		#endregion

	}
}