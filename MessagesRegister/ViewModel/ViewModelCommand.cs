﻿using System;
using System.Windows.Input;
using MessagesRegister.Util;

namespace MessagesRegister.ViewModel
{
	public class ViewModelCommand : ViewModelBase, ICommand
	{
		public ViewModelCommand(Action<object> executeAction, Predicate<object> canExecute)
		{
			if (executeAction == null)
				throw new ArgumentNullException("executeAction");
			_executeAction = executeAction;
			_canExecute = canExecute;
		}

		private readonly Predicate<object> _canExecute;
		public bool CanExecute(object parameter)
		{
			return _canExecute == null || _canExecute(parameter);
		}

		private static readonly EventKey CanExecuteChangedEventKey = new EventKey();

		public event EventHandler CanExecuteChanged
		{
			add { EventSet.Add(CanExecuteChangedEventKey, value); }
			remove { EventSet.Remove(CanExecuteChangedEventKey, value); }
		}

		public void OnCanExecuteChanged()
		{
			EventSet.Raise(CanExecuteChangedEventKey, this, EventArgs.Empty);
		}

		private readonly Action<object> _executeAction;

		public void Execute(object parameter)
		{
			_executeAction(parameter);
		} 
	}
}