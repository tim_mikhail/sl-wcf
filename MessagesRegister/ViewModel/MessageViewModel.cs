﻿using System;
using MessagesRegister.Data;
using MessagesRegister.Model;
using MessagesRegister.Util;
using IMessagesService = MessagesRegister.Model.IMessagesService;

namespace MessagesRegister.ViewModel
{
	public class MessageViewModel : ViewModelBase
	{
		private readonly IMessagesService _service;

		private readonly ViewModelCommand _saveCommand;
		private MessageDto _copy;

		public MessageViewModel()
			: this(new MessageService())
        {

        }

		public MessageViewModel(IMessagesService employeeService)
        {
            _service = employeeService;
            _service.SaveMessageError += OnSaveMessageFailed;
            _service.SaveMessageCompleted += OnSaveMessageSuccess;
            
            _saveCommand = new ViewModelCommand(a => RegisterMessage(), a => CanSaveMessage());
        }

		public ViewModelCommand SaveMesageCommand
		{
			get { return _saveCommand; }
		}

		private MessageDto _currentMessage;
		public MessageDto CurrentMessage
		{
			get { return _currentMessage; }
			set
			{
				if (value == _currentMessage) return;

				_currentMessage = value;
				_currentMessage.PropertyChanged += CurrentEmployeePropertyChanged;
			}
		}

		void CurrentEmployeePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			SaveMesageCommand.OnCanExecuteChanged();
		}

		//public void GetMessage(int messageId)
		public void GetMessage()
		{
			CurrentMessage = new MessageDto();
			OnGetMessageSuccess();
			return;
		}

		//public void GetMessage(int messageId)
		//{
		//	if (messageId == 0)
		//	{
		//		CurrentMessage = new MessageDto();
		//		OnGetMessageSuccess();
		//		return;
		//	}
		//	_service.GetMessage(messageId);
		//}

		public void RegisterMessage()
		{
			_service.RegisterMessage(CurrentMessage.AsMessage());
		}
		
		//public void RegisterMessage(string msgTest)
		//{
		//	_service.RegisterMessage(new ServiceReference1.Message { Text = msgTest });
		//}
		
		bool CanSaveMessage()
		{
			return true; //TODO: implementation
		}

		static readonly EventKey SaveMessageFailedEventKey = new EventKey();

		public event Action<string> SaveMessageFailed
		{
			add { EventSet.Add(SaveMessageFailedEventKey, value); }
			remove { EventSet.Remove(SaveMessageFailedEventKey, value); }
		}

		void OnSaveMessageFailed(string failReason)
		{
			EventSet.Raise(SaveMessageFailedEventKey, failReason);
		}

		static readonly EventKey SaveMessageSuccessEventKey = new EventKey();

		public event Action SaveMessageSuccess
		{
			add { EventSet.Add(SaveMessageSuccessEventKey, value); }
			remove { EventSet.Remove(SaveMessageSuccessEventKey, value); }
		}

		void OnSaveMessageSuccess()
		{
			EventSet.Raise(SaveMessageSuccessEventKey);
		}
		

		static readonly EventKey GetMessageSuccessEventKey = new EventKey();

		public event Action GetMessageSuccess
		{
			add { EventSet.Add(GetMessageSuccessEventKey, value); }
			remove { EventSet.Remove(GetMessageSuccessEventKey, value); }
		}

		void OnGetMessageSuccess()
		{
			EventSet.Raise(GetMessageSuccessEventKey);
		}
	}
}