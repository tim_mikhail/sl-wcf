﻿using MessagesRegister.Util;

namespace MessagesRegister.ViewModel
{
	public class EventManager
	{
		private readonly EventSet _eventSet = new EventSet();

		protected EventSet EventSet { get { return _eventSet; } }
	}
}