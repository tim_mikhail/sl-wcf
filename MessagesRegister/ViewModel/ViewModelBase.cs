﻿using System.ComponentModel;

namespace MessagesRegister.ViewModel
{
	public abstract class ViewModelBase : EventManager, INotifyPropertyChanged
	{

		protected void RaisePropertyChanged(string property)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(property));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public virtual bool IsValid()
		{
			return true;
		}
	}
}