﻿using System.Windows.Controls;

namespace MessagesRegister
{
	public class AppHelper : Page
	{
		public static void ShowMessage(string message, string detail)
		{
			new ErrorWindow(message, detail).Show();
		}
	}
}