﻿using MessagesRegister.ServiceReference1;

namespace MessagesRegister.Data
{
	public static class MessageExtension
	{
		public static MessageDto AsMessageDto(this ServiceReference1.Message msg)
		{
			return new MessageDto
			{
				Id = msg.Id,
				Text = msg.Text,
				RegDate = msg.RegDate,
			};
		}
	}
}