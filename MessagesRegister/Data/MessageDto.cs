﻿using System;
using System.ComponentModel.DataAnnotations;
using MessagesRegister.ServiceReference1;
using MessagesRegister.ViewModel;

namespace MessagesRegister.Data
{
	public class MessageDto : ViewModelBase, ICloneable<MessageDto>
	{
		private string _name;

		public MessageDto()
		{
			_name = string.Empty;
		}

		public ServiceReference1.Message AsMessage()
		{
			return new ServiceReference1.Message
			{
				Id = Id,
				Text = Text,
				RegDate = RegDate,

			};
		}

		[Display(Name = "Идентификатор")]
		public Guid Id { get; set; }
		
		[Required]
		[Display(Name = "Текст", Description = "Введите текст")]
		public String Text
		{
			get { return _name; }
			set
			{
				if (value == _name) return;
				_name = value;
				RaisePropertyChanged("Name");
			}
		}

		[Display(Name = "Дата время регистрации")]
		public DateTime RegDate { get; set; }

		#region ICloneable<EmployeeDto> Members

		public MessageDto Clone()
		{
			return MemberwiseClone() as MessageDto;
		}

		#endregion
	}

	public interface ICloneable<out T>
	{
		T Clone();
	}
}
