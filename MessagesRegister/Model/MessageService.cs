﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MessagesRegister.ServiceReference1;
using MessagesRegister.Util;

namespace MessagesRegister.Model
{
	public class MessageService : EventManager, IMessagesService
	{
		  private readonly MessagesServiceClient _serviceClient;

        public MessageService()
        {
            _serviceClient = new MessagesServiceClient();
            _serviceClient.GetCompleted += _serviceClient_GetCompleted;
            _serviceClient.RegisterCompleted += _serviceClient_RegisterCompleted;
        }
		
		public void GetMessages()
		{
			_serviceClient.GetAsync();
		}

		public void RegisterMessage(ServiceReference1.Message msg)
		{
			_serviceClient.RegisterAsync(msg);
		}
		
		private void _serviceClient_RegisterCompleted(object sender, AsyncCompletedEventArgs e)
		{
			if (e.Error != null)
			{
				OnSaveEmployeeError(e.Error.Message);
				return;
			}

			//OnSaveMessageCompleted(e.Result);
			OnSaveMessageCompleted();
		}

		private void _serviceClient_GetCompleted(object sender, GetCompletedEventArgs e)
		{
			if (e.Error != null)
            {
                OnGetMessagesError(e.Error.Message);
                return;
            }
            
            OnGetMessageCompleted(e.Result);
		}

		#region Events
		

		static readonly EventKey GetMsgsCompletedEventKey = new EventKey();
		public event Action<string> GetMessagesError;
		public event Action<string> SaveMessageError;

		public event Action<ObservableCollection<ServiceReference1.Message>> GetMessageCompleted
		{
			add { EventSet.Add(GetMsgsCompletedEventKey, value); }
			remove { EventSet.Remove(GetMsgsCompletedEventKey, value); }
		}

		void OnGetMessageCompleted(ObservableCollection<ServiceReference1.Message> msgs)
		{
			EventSet.Raise(GetMsgsCompletedEventKey, msgs);
		}

		static readonly EventKey GetMessageErrorEventKey = new EventKey();
		public event Action<string> GetEmployeesError
		{
			add { EventSet.Add(GetMessageErrorEventKey, value); }
			remove { EventSet.Remove(GetMessageErrorEventKey, value); }
		}

		void OnGetMessagesError(string failReason)
		{
			EventSet.Raise(GetMessageErrorEventKey, failReason);
		}
		
		static readonly EventKey SaveMessageCompletedEventKey = new EventKey();
		public event Action SaveMessageCompleted
		{
			add { EventSet.Add(SaveMessageCompletedEventKey, value); }
			remove { EventSet.Remove(SaveMessageCompletedEventKey, value); }
		}

		

		void OnSaveMessageCompleted()
		{
			EventSet.Raise(SaveMessageCompletedEventKey);
		}

		static readonly EventKey SaveMessageErrorEventKey = new EventKey();
		public event Action<string> SaveEmployeeError
		{
			add { EventSet.Add(SaveMessageErrorEventKey, value); }
			remove { EventSet.Remove(SaveMessageErrorEventKey, value); }
		}

		void OnSaveEmployeeError(string failReason)
		{
			EventSet.Raise(SaveMessageErrorEventKey, failReason);
		}


		#endregion

		~MessageService()
        {
			_serviceClient.GetCompleted -= _serviceClient_GetCompleted;
			_serviceClient.RegisterCompleted -= _serviceClient_RegisterCompleted;
            _serviceClient.CloseAsync();
        }

	}
}