﻿using System;
using System.Collections.ObjectModel;
using MessagesRegister.ServiceReference1;

namespace MessagesRegister.Model
{
	public interface IMessagesService
	{
		event Action<string> GetMessagesError;
		event Action<string> SaveMessageError;

		event Action<ObservableCollection<ServiceReference1.Message>> GetMessageCompleted;
		event Action SaveMessageCompleted;

		void GetMessages();
		void RegisterMessage(ServiceReference1.Message msg);
	}
}