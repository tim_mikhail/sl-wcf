﻿namespace MessagesRegister.Util
{
	public abstract class EventManager
	{
		private readonly EventSet _eventSet = new EventSet();

		protected EventSet EventSet { get { return _eventSet; } }

	}
}
