﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace MessagesRegister.Util
{

	//(NOTE : This class is taken from the book CLR Via C# 3rd Edition by Jeffrey Richter (Chapter 11 : Events))

	// This class exists to provide a bit more type safety and
	// code maintainability when using EventSet
	public sealed class EventKey : Object { }

	//NOTE : This class is taken from the book CLR Via C# 3rd Edition by Jeffrey Richter (Chapter 11 : Events)
	public sealed class EventSet
	{

		/// <summary>
		/// The private dictionary used to maintain EventKey -> Delegate mappings
		/// </summary>
		private readonly Dictionary<EventKey, Delegate> _mEvents = new Dictionary<EventKey, Delegate>();


		/// <summary>
		/// Adds an EventKey -> Delegate mapping if it doesn't exist or
		/// combines a delegate to an existing EventKey
		/// </summary>
		/// <param name="eventKey"></param>
		/// <param name="handler"></param>
		public void Add(EventKey eventKey, Delegate handler)
		{
			Monitor.Enter(_mEvents);
			Delegate d;
			_mEvents.TryGetValue(eventKey, out d);
			_mEvents[eventKey] = Delegate.Combine(d, handler);
			Monitor.Exit(_mEvents);
		}


		/// <summary>
		/// Removes a delegate from an EventKey (if it exists) and
		/// removes the EventKey -> Delegate mapping the last delegate is removed
		/// </summary>
		/// <param name="eventKey"></param>
		/// <param name="handler"></param>
		public void Remove(EventKey eventKey, Delegate handler)
		{
			Monitor.Enter(_mEvents);
			// Call TryGetValue to ensure that an exception is not thrown if
			// attempting to remove a delegate from an EventKey not in the set
			Delegate d;
			if (_mEvents.TryGetValue(eventKey, out d))
			{
				d = Delegate.Remove(d, handler);
				// If a delegate remains, set the new head else remove the EventKey
				if (d != null) _mEvents[eventKey] = d;
				else _mEvents.Remove(eventKey);
			}
			Monitor.Exit(_mEvents);
		}


		/// <summary>
		///  Raises the event for the indicated EventKey 
		/// </summary>
		/// <param name="eventKey"></param>
		/// <param name="values"> </param>
		public void Raise(EventKey eventKey, params object[] values)
		{
			// Don't throw an exception if the EventKey is not in the set
			Delegate d;
			Monitor.Enter(_mEvents);
			_mEvents.TryGetValue(eventKey, out d);
			Monitor.Exit(_mEvents);
			if (d != null)
			{
				// Because the dictionary can contain several different delegate types,
				// it is impossible to construct a type-safe call to the delegate at
				// compile time. So, I call the System.Delegate type's DynamicInvoke
				// method, passing it the callback method's parameters as an array of

				// objects. Internally, DynamicInvoke will check the type safety of the
				// parameters with the callback method being called and call the method.
				// If there is a type mismatch, then DynamicInvoke will throw an exception.
				d.DynamicInvoke(values);
			}
		}
	}

}
